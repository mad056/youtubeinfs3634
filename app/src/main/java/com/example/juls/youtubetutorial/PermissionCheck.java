package com.example.juls.youtubetutorial;

import android.content.Context;
import android.net.ConnectivityManager;

public class PermissionCheck {

    // We need interent.
    // So we check the internet permissions.
    // If we have none, don't let the user open the program.
    // Sucks cause no offline access, but is good because hte user can access from anyones phone, anywhere!

    public boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
