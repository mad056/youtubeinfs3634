package com.example.juls.youtubetutorial;

        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.support.annotation.NonNull;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.EditText;
        import android.widget.Spinner;
        import android.widget.Toast;

        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;

        import java.util.ArrayList;

        import static android.widget.Toast.LENGTH_LONG;

public class DeckEditSelect extends Activity {
    Spinner SubjectSpinner;
    Spinner DeckSpinner;
    Login login = new Login();
    ArrayAdapter<String> SubjectAdapter;
    ArrayAdapter<String> DeckAdapter;
    java.util.ArrayList<String> SubjectString;
    java.util.ArrayList<String> DeckString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_edit_select);

        SubjectSpinner = findViewById(R.id.EditSubjectSpinner);
        SubjectString = new java.util.ArrayList<>();
        SubjectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SubjectString);
        SubjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(SubjectAdapter);

        DeckSpinner = findViewById(R.id.EditDeckSpinner);
        DeckString = new java.util.ArrayList<>();
        DeckAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DeckString);
        DeckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DeckSpinner.setAdapter(DeckAdapter);


        LoadSubjectSpinner();

        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DeckString.clear();
                RefreshDecks(SubjectSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }


    public void LoadSubjectSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    SubjectString.add(InnerInnerSnapShot.getKey());
                                    SubjectAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }


    // While the load subjects is static, the decks however are dynamic. Thus we pass through the subject and grab its decks.
    // Reference the onchange listener for the spinner above in the oncreate;.

    // THERE ARE MANY FOR LOOPS
    // FOR A CLEANER VERSION
    // LOOK AT EDITOR.JAVA
    // LOWKEY JUST TOO LAZY TO CHANGE THIS
    public void RefreshDecks(final String Subject) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    if (InnerInnerSnapShot.getKey().equals(Subject)) {
                                        // Oh no were doing to deep! We now are iterating through the decks and total deck count.
                                        for (DataSnapshot DeckSnapShot : InnerInnerSnapShot.getChildren()) {
                                            if (!DeckSnapShot.getKey().equals("Total Decks")) {
                                                // Add them to the decklist
                                                // Update the deck spinner
                                                DeckString.add(DeckSnapShot.getKey());
                                                DeckAdapter.notifyDataSetChanged();
                                            }

                                        }
                                        }
                                    }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

public void DeleteOnClick(View v) {
        try {
            CheckTotalDeckCount(SubjectSpinner.getSelectedItem().toString());

        } catch (NullPointerException e) {
            Toast.makeText(DeckEditSelect.this, "Nothing Selected!", LENGTH_LONG).show();

        }
}


    public void CheckTotalDeckCount(final String Subject) {
        // The idea is that if a subject has no decks we can prompt the user to delete the whole subject

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + login.CurrentUserID + "/Subjects/" + Subject).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals("Total Decks")) {
                        // it equals 1 because we haven't deleted the subject yet. we want to check if the user wants to delete the entire subject
                        if (answerSnapshot.getValue(Integer.class) == 1) {
                            // prompt user
                            DeleteSubjectPrompt(Subject);
                        } else {
                            // >1 deck just delete the deck
                            DeleteDeck(Subject, DeckSpinner.getSelectedItem().toString());
                        }


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    public void DeleteSubjectPrompt(final String Subject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.SubjectDeletion);
        builder.setMessage(R.string.SubjectDeletionConfirm);

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                DeleteSubject(Subject);
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteDeck(Subject, DeckSpinner.getSelectedItem().toString());
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }
    public void DeleteSubject(String Subject) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject );
        mdatabaseReference.removeValue();

    }

    public void DeleteDeck(String Subject, String Deck) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck);
        mdatabaseReference.removeValue();
        GrabDeckToUpdate(Subject);
    }

    public void GrabDeckToUpdate(final String Subject) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + login.CurrentUserID + "/Subjects/" + Subject).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals("Total Decks")) {
                        SetDeckUpdate(Subject, answerSnapshot.getValue(Integer.class)) ;


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SetDeckUpdate(String Subject, int TotalDecks) {
        // Same as create Deck

        TotalDecks = TotalDecks - 1;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject );
        mdatabaseReference.child("Total Decks").setValue(TotalDecks);
        System.out.println(String.valueOf("Second " + TotalDecks));

    }

    public void EditDeckButton(View v) {
        try {
            Intent intent = new Intent(getBaseContext(), Editor.class);
            intent.putExtra("Subject", SubjectSpinner.getSelectedItem().toString());
            intent.putExtra("Deck", DeckSpinner.getSelectedItem().toString());
            startActivity(intent);
        } catch (NullPointerException e) {
            Toast.makeText(DeckEditSelect.this, "Nothing Selected!", LENGTH_LONG).show();

        }

    }
}
