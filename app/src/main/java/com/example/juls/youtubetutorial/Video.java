package com.example.juls.youtubetutorial;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Video {

    // ITEMS FOR GSON.
    // I have included a sample output for the youtube playlist so you understand the GSON format. Check the ZIP File.

    @SerializedName("items")
    ArrayList<items> itemlist;
    @SerializedName("kind")  String kind;



    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public ArrayList<items> getItemlist() {
        return itemlist;
    }

    public void setItemlist(ArrayList<items> itemlist) {
        this.itemlist = itemlist;
    }
}

