package com.example.juls.youtubetutorial;

public class CardItemRecyclerView {

    private String Line1;
    private String Line2;
    private String NumberText;
    private String Subject;
    private String Deck;

    public CardItemRecyclerView(String mLine1, String mLine2, String mNumberText, String mSubject, String mDeck) {

        // Default constructor for the recycle view. Card item class.
        Line1 = mLine1;
        Line2 = mLine2;
        NumberText = mNumberText;
        Subject = mSubject;
        Deck = mDeck;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getDeck() {
        return Deck;
    }

    public void setDeck(String deck) {
        Deck = deck;
    }

    public String getLine1() {
        return Line1;
    }

    public void setLine1(String line1) {
        Line1 = line1;
    }

    public String getLine2() {
        return Line2;
    }

    public void setLine2(String line2) {
        Line2 = line2;
    }

    public String getNumberText() {
        return NumberText;
    }

    public void setNumberText(String numberText) {
        NumberText = numberText;
    }



}
