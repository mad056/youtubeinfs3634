package com.example.juls.youtubetutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.logging.Logger;

public class CardListViewFragment  extends Fragment {
    Login login = new Login();

    private RecyclerView mRecycleView;
    private RecyclerView.Adapter mAdapter;
    ArrayList<CardItemRecyclerView> CardList;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final Logger LOGGER = Logger.getLogger(Editor.class.getName());
    String Subject;
    String Deck;
    public CardListViewFragment(){};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View PageTwo = inflater.inflate(R.layout.cardviewfragmentlayout, container, false);

        if (getArguments() != null) {
            Subject = getArguments().getString("Subject");
            Deck = getArguments().getString("Deck");

        }


        // Initialize the adapter
        CardList = new ArrayList<>();
        mRecycleView = PageTwo.findViewById(R.id.CardViewFragmentRecycler);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new CardItemAdapter(CardList);
        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);

        // Adding a divider to each item.
        mRecycleView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        LoadData(Subject, Deck);

        return PageTwo;
    }


    public void LoadData(final String mSubject, final String mDeck) {
        // You might be thinking...
        // Why so many for loops.
        // Now i definiely can just go stright to the Reference.
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + mSubject + "/" + mDeck );

        MaxDatabase.child("Questions").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CardList.clear();
                int tempint = 0;

                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    tempint += 1;
                    // Set it for our recycler view..nice and long
                    LOGGER.info(" QUESTION FOUND" + answerSnapshot.getKey());

                    CardItemRecyclerView tempitem = new CardItemRecyclerView(answerSnapshot.getKey(), answerSnapshot.getValue(String.class), String.valueOf(tempint), "Youtube", "Youtube");
                    CardList.add(tempitem);
                    // Debug

                }
                mAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}
