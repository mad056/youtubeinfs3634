package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.widget.Toast.LENGTH_LONG;

public class InteractiveDeckPlaylistSelect extends Activity {
    Spinner SubjectSpinner;
    Spinner PlayListSpinner;

    Spinner DeckSpinner;
    Login login = new Login();
    ArrayAdapter<String> SubjectAdapter;
    ArrayAdapter<String> PlayListAdapter;

    ArrayAdapter<String> DeckAdapter;
    java.util.ArrayList<String> SubjectString;
    java.util.ArrayList<String> DeckString;
    java.util.ArrayList<String> PlayListString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This whole class just sets adapters and spinners for deck and subjects in the database
        // we also grab the playlists from the database
        // The only available one right now is the test INFS3617

        setContentView(R.layout.activity_interactive_deck_playlist_select);
        SubjectSpinner = findViewById(R.id.InteractiveSubjectSpinner);
        SubjectString = new java.util.ArrayList<>();
        SubjectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SubjectString);
        SubjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(SubjectAdapter);

        PlayListSpinner = findViewById(R.id.InteractivePlaylistSpinner);
        PlayListString = new java.util.ArrayList<>();
        PlayListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, PlayListString);
        PlayListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        PlayListSpinner.setAdapter(PlayListAdapter);

        DeckSpinner = findViewById(R.id.InteractiveDeckSpinner);
        DeckString = new java.util.ArrayList<>();
        DeckAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DeckString);
        DeckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DeckSpinner.setAdapter(DeckAdapter);


        LoadSubjectSpinner();
        LoadPlaylistSpinner();
        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DeckString.clear();
                RefreshDecks(SubjectSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }


    public void LoadSubjectSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    SubjectString.add(InnerInnerSnapShot.getKey());
                                    SubjectAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    public void LoadPlaylistSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Playlist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                                    PlayListString.add(answerSnapshot.getKey());
                    PlayListAdapter.notifyDataSetChanged();





                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    // While the load subjects is static, the decks however are dynamic. Thus we pass through the subject and grab its decks.
    // Reference the onchange listener for the spinner above in the oncreate;.

    // THERE ARE MANY FOR LOOPS
    // FOR A CLEANER VERSION
    // LOOK AT EDITOR.JAVA
    // LOWKEY JUST TOO LAZY TO CHANGE THIS
    public void RefreshDecks(final String Subject) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    if (InnerInnerSnapShot.getKey().equals(Subject)) {
                                        // Oh no were doing to deep! We now are iterating through the decks and total deck count.
                                        for (DataSnapshot DeckSnapShot : InnerInnerSnapShot.getChildren()) {
                                            if (!DeckSnapShot.getKey().equals("Total Decks")) {
                                                // Add them to the decklist
                                                // Update the deck spinner
                                                DeckString.add(DeckSnapShot.getKey());
                                                DeckAdapter.notifyDataSetChanged();
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void InteractiveStudyOnClick(View v) {
        try {
            GrabPlaylistAndStart();

        } catch (NullPointerException e) {
            Toast.makeText(InteractiveDeckPlaylistSelect.this, "Nothing Selected!", LENGTH_LONG).show();

        }
    }


    public void GrabPlaylistAndStart() {
        // Here we grab the playlist ID and start the interactive study
        try {
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();

            MaxDatabase.child("Playlist").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        try {
                            if (answerSnapshot.getKey().equals(PlayListSpinner.getSelectedItem().toString())) {
                                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                intent.putExtra("Subject", SubjectSpinner.getSelectedItem().toString());
                                intent.putExtra("Deck", DeckSpinner.getSelectedItem().toString());
                                intent.putExtra("Playlist", PlayListSpinner.getSelectedItem().toString());
                                intent.putExtra("PlaylistID", answerSnapshot.getValue(String.class));

                                startActivity(intent);
                            }

                        } catch (NullPointerException e) {
                            Toast.makeText(InteractiveDeckPlaylistSelect.this, "Nothing Selected!", LENGTH_LONG).show();

                        }






                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } catch (NullPointerException e) {
            Toast.makeText(InteractiveDeckPlaylistSelect.this, "Nothing Selected!", LENGTH_LONG).show();

        }

        }



}
