package com.example.juls.youtubetutorial;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class items {
    // ITEMS FOR GSON.
    // I have included a sample output for the youtube playlist so you understand the GSON format. Check the ZIP File.


    @SerializedName("id")    String id;
    @SerializedName("kind")  String kind;
    @SerializedName("snippet")
    snippet snippet;


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public com.example.juls.youtubetutorial.snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(com.example.juls.youtubetutorial.snippet snippet) {
        this.snippet = snippet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideoId() {
        return kind;
    }

    public void setVideoId(String videoId) {
        this.kind = videoId;
    }


}
