package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.widget.Toast.LENGTH_LONG;

public class DeckSelectStudy extends Activity {
    Spinner SubjectSpinner;
    Spinner DeckSpinner;
    Login login = new Login();
    ArrayAdapter<String> SubjectAdapter;
    ArrayAdapter<String> DeckAdapter;
    java.util.ArrayList<String> SubjectString;
    java.util.ArrayList<String> DeckString;

    // Quite literally the same class as the deck edit select. I can't be bothered to Change the other class to be used for two purposes.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_select_study);
        SubjectSpinner = findViewById(R.id.StudySubjectSpinner);
        SubjectString = new java.util.ArrayList<>();
        SubjectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SubjectString);
        SubjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(SubjectAdapter);

        DeckSpinner = findViewById(R.id.StudyDeckSpinner);
        DeckString = new java.util.ArrayList<>();
        DeckAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DeckString);
        DeckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DeckSpinner.setAdapter(DeckAdapter);


        LoadSubjectSpinner();
        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DeckString.clear();
                RefreshDecks(SubjectSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }


    public void LoadSubjectSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    SubjectString.add(InnerInnerSnapShot.getKey());
                                    SubjectAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    // While the load subjects is static, the decks however are dynamic. Thus we pass through the subject and grab its decks.
    // Reference the onchange listener for the spinner above in the oncreate;.

    // THERE ARE MANY FOR LOOPS
    // FOR A CLEANER VERSION
    // LOOK AT EDITOR.JAVA
    // LOWKEY JUST TOO LAZY TO CHANGE THIS
    public void RefreshDecks(final String Subject) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    if (InnerInnerSnapShot.getKey().equals(Subject)) {
                                        // Oh no were doing to deep! We now are iterating through the decks and total deck count.
                                        for (DataSnapshot DeckSnapShot : InnerInnerSnapShot.getChildren()) {
                                            if (!DeckSnapShot.getKey().equals("Total Decks")) {
                                                // Add them to the decklist
                                                // Update the deck spinner
                                                DeckString.add(DeckSnapShot.getKey());
                                                DeckAdapter.notifyDataSetChanged();
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void StudyOnClick(View v) {

        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + SubjectSpinner.getSelectedItem().toString());

            MaxDatabase.child( DeckSpinner.getSelectedItem().toString()).addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        if (answerSnapshot.getKey().equals("Total Cards")) {
                            if (answerSnapshot.getValue(Integer.class) == 0) {
                                Toast.makeText(DeckSelectStudy.this, "Deck size is 0. Please select another deck..", LENGTH_LONG).show();

                            } else {
                                // if it isn't we start the studier
                                Intent intent = new Intent(getBaseContext(), Study.class);
                                intent.putExtra("Subject", SubjectSpinner.getSelectedItem().toString());
                                intent.putExtra("Deck", DeckSpinner.getSelectedItem().toString());
                                startActivity(intent);
                            }
                        }

                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

        } catch (NullPointerException e) {
            Toast.makeText(DeckSelectStudy.this, "Nothing Selected!", LENGTH_LONG).show();

        }
        // so we cehck the subject card count. if the count is 0 we can't let the user study.


    }
}
