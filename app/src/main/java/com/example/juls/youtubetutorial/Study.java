package com.example.juls.youtubetutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import org.apache.commons.text.similarity.CosineDistance;
import org.apache.commons.text.similarity.FuzzyScore;
import org.apache.commons.text.similarity.LevenshteinDistance;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

import static android.widget.Toast.LENGTH_LONG;

public class Study extends AppCompatActivity {
    String Subject;
    String Deck;
    Login login = new Login();
    int RandomQuestion;
    ArrayList<Question> QuestionList;
    InputChecksEssentials minputcheckessentials = new InputChecksEssentials();
    private static final Logger LOGGER = Logger.getLogger(Study.class.getName());

    TextView StudyQuestionDynamic;
    EditText AnwerMultiLineText;
    TextView StudyCardNumberDynamic;
    TextView StudyCardAccuracy;
    TextView StudyCardCorrect;
    Question CurrentQuestion;

    Button QuitButton;
    Button CheckButton;
    Button ShowButton;
    int CardNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);
        Subject = getIntent().getStringExtra("Subject");
        Deck = getIntent().getStringExtra("Deck");
        StudyQuestionDynamic = findViewById(R.id.StudyQuestionDynamic);
        AnwerMultiLineText = findViewById(R.id.AnwerMultiLineText);
        StudyCardNumberDynamic = findViewById(R.id.StudyCardNumberDynamic);
        StudyCardAccuracy = findViewById(R.id.StudyCardAccuracy);
        StudyCardCorrect = findViewById(R.id.StudyCardCorrect);
        QuitButton = findViewById(R.id.quitStudyButton);
        CheckButton = findViewById(R.id.CheckStudyButton);
        ShowButton = findViewById(R.id.ShowStudyButton);

        QuestionList = new ArrayList<>();

        LoadData(Deck, Subject);
    }

    public void LoadData(String mDeck, String mSubject) {

        // Load questions
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + mSubject + "/" + mDeck );

        MaxDatabase.child("Questions").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                QuestionList.clear();
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    // Set it for our recycler view..nice and long
                    LOGGER.info(" QUESTION FOUND" + answerSnapshot.getKey());

                    Question tempitem = new Question();
                    tempitem.setAnswer(answerSnapshot.getValue(String.class));
                    tempitem.setQuestion(answerSnapshot.getKey());
                    QuestionList.add(tempitem);
                    // Debug

                }
                StudyCardNumberDynamic.setText(String.valueOf(QuestionList.size()) + " Card Left");
                StudyCardAccuracy.setText("Your Accuracy will Show Here");
                StudyCardCorrect.setText("Waiting for answer...");
                LoadQuestion();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public int getRandom() {
        // Random int of the question list size.
        Random r = new Random();
        int temprandom;
       temprandom = (r.nextInt(QuestionList.size()));
       return temprandom;
    }

    public void SetCardText() {

    }




    public void LoadQuestion() {

        // load a random int and question
        // reset all the items on the page
        QuitButton.setEnabled(true);
        CheckButton.setText("Check");
        ShowButton.setEnabled(true);
        StudyCardAccuracy.setText("Waiting...");
        StudyCardCorrect.setText("Waiting...");

        AnwerMultiLineText.setEnabled(true);
        AnwerMultiLineText.setText("");
        RandomQuestion = getRandom();
        CurrentQuestion = QuestionList.get(RandomQuestion);
        StudyQuestionDynamic.setText(CurrentQuestion.getQuestion());


    }

    public void StudyCheckonclick(View v) {
        double Correctness = 0;

        if (CheckButton.getText().toString().equals("Check")) {
            LevenshteinDistance tester = new LevenshteinDistance();
            if (!minputcheckessentials.nonNull(AnwerMultiLineText.getText().toString())) {
                QuitButton.setEnabled(false);
                CheckButton.setText("Next");
                ShowButton.setEnabled(false);


                // We are using the LevenshteinDistance of apache commons.text libary to check the text similarity.
                // We change the text to a charsequence
                CharSequence USerAnswer = AnwerMultiLineText.getText().toString();
                CharSequence TheAnswer =  CurrentQuestion.getAnswer();
                /// We work magic then turn the decimal format to a popper digit.
                Correctness = tester.apply(TheAnswer,USerAnswer);
                Log.d("Study", String.valueOf(Correctness));

                if (Correctness < 0) {
                    Correctness = 10;
                    Log.d("Study", "Below 0");

                }

                if (Correctness > 10) {
                    Correctness = 10;
                    Log.d("Study", "Above 10");

                }

                Correctness = (Correctness / 10) * 100;
                Log.d("Study", String.valueOf(Correctness));

                Correctness = 100 - Correctness;

                Log.d("Study", String.valueOf(Correctness));

                int TempCorrect = (int) Math.round(Correctness);
                Log.d("StudyFinal", String.valueOf(TempCorrect));

                // In CosineDistance. 100 is incorrect and 0 is correct. So instead of changing it..... we just do this.
                if (TempCorrect < 70) {
                    StudyCardCorrect.setText("Incorrect");
                    StudyCardCorrect.setTextColor(getResources().getColor(R.color.red));

                } else {
                    // Only 70% and over gets to be correct.
                    StudyCardCorrect.setText("Correct");

                    QuestionList.remove(RandomQuestion);
                    StudyCardCorrect.setTextColor(getResources().getColor(R.color.green));

                }


                StudyCardAccuracy.setText("You were " + String.valueOf(Correctness) + " % Correct");
                AnwerMultiLineText.setEnabled(false);
                AnwerMultiLineText.setText(CurrentQuestion.getAnswer());
                StudyCardNumberDynamic.setText(String.valueOf(QuestionList.size()) + " Card Left");

            } else {
                Toast.makeText(Study.this, "Please enter a answer.", LENGTH_LONG).show();
            }
        } else {
            if(QuestionList.size() == 0) {
                Toast.makeText(Study.this, "Study Finished", LENGTH_LONG).show();
                QuitButton.setEnabled(true);

            } else {
                ShowButton.setText("Show");

                LoadQuestion();

            }
        }



    }


    public void StudyShowonclick(View v) {

        if (ShowButton.getText().equals("Skip")) {
            QuestionList.remove(RandomQuestion);
            LoadQuestion();
            ShowButton.setText("Show");
            StudyCardNumberDynamic.setText(String.valueOf(QuestionList.size()) + " Card's Left");


        } else {

            QuitButton.setEnabled(false);
            CheckButton.setText("Next");
            StudyCardAccuracy.setText("No Accuracy");
            AnwerMultiLineText.setEnabled(false);
            AnwerMultiLineText.setText(CurrentQuestion.getAnswer());
            StudyCardNumberDynamic.setText(String.valueOf(QuestionList.size()) + " Card's Left");
            ShowButton.setText("Skip");
        }
    }


    public void StudyQuitonclick(View v) {
        Intent intent2 = new Intent(this, MainPage.class);
        startActivity(intent2);


    }
}
