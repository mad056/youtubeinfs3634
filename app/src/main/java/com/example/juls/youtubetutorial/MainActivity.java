package com.example.juls.youtubetutorial;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;


public class MainActivity extends AppCompatActivity  implements YouTubePlayer.OnInitializedListener  {    private static final int RECOVERY_REQUEST = 1;
    private TextView Status;
    private BottomSheetBehavior mBottomSheetBehavior;
    private MyPlaybackEventListener playbackEventListener;
    private TextView mTextViewState;
    private YouTubePlayerSupportFragment youTubeView;
    Login login = new Login();
    @BindView(R.id.bottom_sheet)
    View bottomSheet;
    TabLayout MyTabs;
    ViewPager MyPage;
    public static Video data;
    String Deck;
    String Subject;
    String Playlist;
    String PlayListID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        playbackEventListener = new MyPlaybackEventListener();
        setContentView(R.layout.mainviewfinal);
        ButterKnife.bind(this);
        Status = findViewById(R.id.Status);

        Playlist = getIntent().getStringExtra("Playlist");
        PlayListID = getIntent().getStringExtra("PlaylistID");

        Subject = getIntent().getStringExtra("Subject");
        Deck = getIntent().getStringExtra("Deck");
        // youTubeView.initialize(Config.YOUTUBE_API_KEY, MainActivity.this);
        //   Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        // Setup ViewPager
     //Log.d("MainActivityYoutube", String.valueOf(data.kind));

        MyTabs = (TabLayout) findViewById(R.id.MyTabs);
        MyPage = (ViewPager) findViewById(R.id.YoutubeViewPager);

        MyTabs.setupWithViewPager(MyPage);
        SetUpViewPager(MyPage);

        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mTextViewState = findViewById(R.id.text_view_state);

        Button buttonExpand = findViewById(R.id.button_expand);
        Button buttonCollapse = findViewById(R.id.button_collapse);
        Button AddButton = findViewById(R.id.AddCardInteractiveButton);

        youTubeView =
                (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_view);
        youTubeView.initialize(Config.YOUTUBE_API_KEY, this);

        AddButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText QuestionText = findViewById(R.id.InteractiveQuestionText);
                EditText AnswerText = findViewById(R.id.InteractiveAnswerText);

                InputChecksEssentials Check = new InputChecksEssentials();
                boolean QuestionBoolean = Check.nonNull(QuestionText.getText().toString());
                boolean AnswerBoolean = Check.nonNull(AnswerText.getText().toString());
                if (QuestionBoolean == false && AnswerBoolean == false) {

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck + "/Questions");
                    mdatabaseReference.child(QuestionText.getText().toString()).setValue(AnswerText.getText().toString());
                    Toast.makeText(MainActivity.this, "Added", Toast.LENGTH_SHORT).show();
                    GrabDeckToUpdate(Subject, Deck);

                } else {
                    Toast.makeText(MainActivity.this, "Please Check Your Inputs!", LENGTH_LONG).show();

                }
                QuestionText.setText("");
                AnswerText.setText("");
            }
        });


        buttonExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        buttonCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        mTextViewState.setText("Collapsed");
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        mTextViewState.setText("Dragging...");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        mTextViewState.setText("Expanded");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        mTextViewState.setText("Hidden");
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        mTextViewState.setText("Settling...");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                mTextViewState.setText("Sliding...");
            }
        });
    }














    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        player.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
           /// player.cueVideo("h-HlBPx8ek4");
        player.cuePlaylist(PlayListID);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    public void SetUpViewPager (ViewPager viewpage){
        MyViewPageAdapter Adapter = new MyViewPageAdapter(getSupportFragmentManager());

        Adapter.AddFragmentPage(new CardListViewFragment(), "Questions");
        Adapter.AddFragmentPage(new YoutubePlaylistFragment(), "Playlist");

        //We Need Fragment class now

        viewpage.setAdapter(Adapter);

    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
         //  RecyclerView  MyPage.findViewById()
        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    }


        final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {


    @Override
    public void onPlaying() {
        Status.setText("Playing");
    }

    @Override
    public void onPaused() {
        Status.setText("Paused");
    }

    @Override
    public void onStopped() {
        Status.setText("Stopped");

    }

    @Override
    public void onBuffering(boolean b) {
        Status.setText("Buffering");

    }

    @Override
    public void onSeekTo(int i) {

    }
}


    public class MyViewPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> MyFragment = new ArrayList<>();
        private List<String> MyPageTittle = new ArrayList<>();

        public MyViewPageAdapter(FragmentManager manager){
            super(manager);
        }

        public void AddFragmentPage(Fragment Frag, String Title){
            MyFragment.add(Frag);
            MyPageTittle.add(Title);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Bundle bundle = new Bundle();
                    CardListViewFragment CardListViewFragment = new CardListViewFragment();
                    bundle.putString("Subject", Subject);
                    bundle.putString("Deck", Deck);
                    CardListViewFragment.setArguments(bundle);
                    return CardListViewFragment;
                case 1:
                    YoutubePlaylistFragment YoutubePlaylistFragment = new YoutubePlaylistFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("Playlist", Playlist);
                    YoutubePlaylistFragment.setArguments(bundle2);
                    return YoutubePlaylistFragment;

            }

            return MyFragment.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return MyPageTittle.get(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       // if (id == R.id.action_settings) {
      //      return true;
     //   }

        return super.onOptionsItemSelected(item);
    }




    public void GrabDeckToUpdate(final String Subject, final String Deck) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + Login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals("Total Cards")) {
                        Log.d("CardUp", "SetDeckUpdate");
                        SetDeckUpdate(Subject, answerSnapshot.getValue(Integer.class), Deck) ;


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SetDeckUpdate(String Subject, int TotalDecks, String Deck) {
        Log.d("Create", "SetDeckUpdateInwards");

        TotalDecks = TotalDecks + 1;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck);
        mdatabaseReference.child("Total Cards").setValue(TotalDecks);

    }



}
