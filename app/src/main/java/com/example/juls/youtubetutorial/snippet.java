package com.example.juls.youtubetutorial;

import com.google.gson.annotations.SerializedName;

public class snippet{
    // ITEMS FOR GSON.
    // I have included a sample output for the youtube playlist so you understand the GSON format. Check the ZIP File.

    @SerializedName("title") String title;
    @SerializedName("description") String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
