package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.logging.Logger;

import static android.widget.Toast.LENGTH_LONG;

public class Editor extends AppCompatActivity {
    Login login = new Login();
String Subject;
String Deck;
ArrayList<CardItemRecyclerView> CardList;
TextView Heading;
    private static final Logger LOGGER = Logger.getLogger(Editor.class.getName());
// Set up the adapters!
private RecyclerView mRecycleView;
private RecyclerView.Adapter mAdapter;
    private Context mContext;
    private RecyclerView.LayoutManager mLayoutManager;



    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        // grab intents set from DeckeditSelect,java
         Subject = getIntent().getStringExtra("Subject");
         Deck = getIntent().getStringExtra("Deck");

         // Setting the heading
         Heading = findViewById(R.id.DeckNameText);
        Heading.setText(Deck);

         // Initialize the adapter
        CardList = new ArrayList<>();
        mRecycleView = findViewById(R.id.EditorRecycleView);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new CardItemAdapter(CardList);
        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);

        // Adding a divider to each item.
        mRecycleView.addItemDecoration(new DividerItemDecoration(Editor.this,
                DividerItemDecoration.VERTICAL));

         // Load the data
        LoadData(Subject, Deck);
    }

    // To do intent the editor on the deckedit
    // Custom Recycler View





    public void LoadData(final String mSubject, final String mDeck) {
        // You might be thinking...
        // Why so many for loops.
        // Now i definiely can just go stright to the Reference.
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + mSubject + "/" + mDeck );

        MaxDatabase.child("Questions").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CardList.clear();
                int tempint = 0;

                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    tempint += 1;
                    // Set it for our recycler view..nice and long
                    LOGGER.info(" QUESTION FOUND" + answerSnapshot.getKey());

                    CardItemRecyclerView tempitem = new CardItemRecyclerView(answerSnapshot.getKey(), answerSnapshot.getValue(String.class), String.valueOf(tempint), mSubject, mDeck);
                    CardList.add(tempitem);
                    // Debug

                }
                mAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }






    public void DeleteCard(final String Name, final String Des, final String Subject, final String Deck) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck );

        MaxDatabase.child("Questions").addListenerForSingleValueEvent(new ValueEventListener() {
            int tempint = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals(Name)) {
                        if (answerSnapshot.getValue(String.class).equals(Des)) {
                            GrabDeckToUpdate(Subject, Deck);
                            answerSnapshot.getRef().removeValue();



                        }
                    }

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void PreviewCard(String Name, String Des, String Subject, String Deck) {
        // Open Question Viewer Layout on this one card.
    }





    public void AddCardButton(View v) {
        Intent intent2 = new Intent(this, EditCard.class);
        intent2.putExtra("Subject", Subject);
        intent2.putExtra("Deck", Deck);
        intent2.putExtra("Name", "NULL");
        intent2.putExtra("Des", "NULL");
        intent2.putExtra("Context", "ADD");

        startActivity(intent2);
    }



    public void GrabDeckToUpdate(final String Subject, final String Deck) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + Login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals("Total Cards")) {
                        Log.d("CardUp", "SetDeckUpdate");
                        SetDeckUpdate(Subject, answerSnapshot.getValue(Integer.class), Deck) ;


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SetDeckUpdate(String Subject, int TotalDecks, String Deck) {


        Log.d("Create", "SetDeckUpdateInwards");


            TotalDecks = TotalDecks - 1;


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck);
        mdatabaseReference.child("Total Cards").setValue(TotalDecks);

    }
}







/* Haha look at this spaghetti
   // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    if (InnerInnerSnapShot.getKey().equals(mSubject)) {
                                        // Oh no were doing to deep! We now are iterating through the decks and total deck count.
                                        for (DataSnapshot DeckSnapShot : InnerInnerSnapShot.getChildren()) {
                                            if (DeckSnapShot.getKey().equals(mDeck)) {
                                                int tempint = 0;
                                               // ONE MORE TIME YEAH! This time were looping through the questions!
                                                for (DataSnapshot QuestionSnapShip : DeckSnapShot.getChildren()) {
                                                    if (QuestionSnapShip.getKey().equals("Questions")) {
                                                        for (DataSnapshot ActualQuestions : QuestionSnapShip.getChildren()) {

                                                        }

                                                    }
                                                }
                                                }

                                        }
                                    }
                                }
                            }

                        }
                    }
 */