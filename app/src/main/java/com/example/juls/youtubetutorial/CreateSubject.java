package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.widget.Toast.LENGTH_LONG;

public class CreateSubject extends Activity {
    EditText SubjectName;
    Login login = new Login();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_subject);

        SubjectName = findViewById(R.id.SubjectNameEditText);

    }

    // Looks familiar. Yeah i am using a similar format to the create deck class.
    // Grab Values, start databsae connection, insert into the right location.
    // From looking at the mdatabaseReference you can get a gist of how my database is structured.
    public void SubjectCreateClick(View v) {
        String TempSubjectName = SubjectName.getText().toString().trim();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + TempSubjectName);
            mdatabaseReference.child("Total Decks").setValue(0);
             Toast.makeText(CreateSubject.this, "Created!", LENGTH_LONG).show();
             finish();

    }

}
