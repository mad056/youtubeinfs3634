package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class TransferDeckSelect extends Activity {
    ProgressDialog dialog;
    Spinner SubjectSpinner;
    EditText Eamil;
    Spinner DeckSpinner;
    Login login = new Login();
    ArrayAdapter<String> SubjectAdapter;
    ArrayAdapter<String> DeckAdapter;
    java.util.ArrayList<String> SubjectString;
    java.util.ArrayList<String> DeckString;
    ArrayList<Question> Questions ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_deck_select);
        dialog = new ProgressDialog(TransferDeckSelect.this);
        SubjectSpinner = findViewById(R.id.TransferSubjectSpinner);
        SubjectString = new java.util.ArrayList<>();
        SubjectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SubjectString);
        SubjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(SubjectAdapter);

        DeckSpinner = findViewById(R.id.TransferDeckDeckSpinner);
        DeckString = new java.util.ArrayList<>();
        DeckAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DeckString);
        DeckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DeckSpinner.setAdapter(DeckAdapter);
        Questions = new ArrayList<>();
        Eamil = findViewById(R.id.TransferDeckEmailEditText);
        LoadSubjectSpinner();



        SubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                DeckString.clear();
                RefreshDecks(SubjectSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

    }

    public void TransferDeckOnClick(View v) {
        try {
            dialog.setMessage("Transferring Please Wait");
            dialog.show();
            dialog.setCancelable(false);
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            String Subject = SubjectSpinner.getSelectedItem().toString();
            String Deck = DeckSpinner.getSelectedItem().toString();
            Log.d("Transfer",  DeckSpinner.getSelectedItem().toString());
            DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck );
            Questions.clear();
            MaxDatabase.child("Questions").addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        Question tempquestion = new Question();
                        tempquestion.setAnswer(answerSnapshot.getValue(String.class));
                        tempquestion.setQuestion(answerSnapshot.getKey());
                        Log.d("TDS: ADDQ", answerSnapshot.getValue(String.class));

                        Questions.add(tempquestion);
                    }
                    if (Questions.size() <= 0) {
                        Toast.makeText(TransferDeckSelect.this, "Deck size is too small to transfer. Please select another deck.", LENGTH_LONG).show();
                        dialog.dismiss();
                    } else {
                        Log.d("TransferDeckSelect", "Step 1 Pass");

                        FindUserID();
                    }
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } catch (NullPointerException e) {
            Toast.makeText(TransferDeckSelect.this, "Nothing Selected!", LENGTH_LONG).show();

        }


    }

    public void FindUserID() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // We we add a check for the user . A simple boolean

                boolean tempcheck = false;
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot emailsnapshot : answerSnapshot.getChildren()) {
                            if (emailsnapshot.getKey().equals("Email")) {
                                if (emailsnapshot.getValue(String.class).equals(Eamil.getText().toString().trim())) {
                                    // hey we found a matching user!
                                    tempcheck = true;
                                    Log.d("TransferDeckSelect", answerSnapshot.getKey());

                                    TransferDeck(answerSnapshot.getKey());
                                }
                            }
                    }
                }
                if (!tempcheck) {
                    // Oh no a user was not found. better inform the sender.
                    Toast.makeText(TransferDeckSelect.this, "User Not Found. Sorry!", LENGTH_LONG).show();
                    dialog.dismiss();

                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void TransferDeck(String UserID) {
        // The final stage. Here we have all the information we need!. So lets iterate over our question list and transfer the data into the other users pending decks.
        Log.d("TransferDeckSelect", "Step 3");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + UserID + "/PendingDeck/" +  DeckSpinner.getSelectedItem().toString());
        for (Question element : Questions) {

           mdatabaseReference.child(element.getQuestion()).setValue(element.getAnswer());


        }
        Toast.makeText(TransferDeckSelect.this, "Transfer Complete", LENGTH_LONG).show();
       dialog.dismiss();
    }


    public void LoadSubjectSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    SubjectString.add(InnerInnerSnapShot.getKey());
                                    SubjectAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


        // While the load subjects is static, the decks however are dynamic. Thus we pass through the subject and grab its decks.
        // Reference the onchange listener for the spinner above in the oncreate;.

        // THERE ARE MANY FOR LOOPS
        // FOR A CLEANER VERSION
        // LOOK AT EDITOR.JAVA
        // LOWKEY JUST TOO LAZY TO CHANGE THIS
        public void RefreshDecks ( final String Subject){
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();

            MaxDatabase.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                        // if the user is our current user
                        if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                            // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                            for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                                if (InnerSnapshop.getKey().equals("Subjects")) {
                                    for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                        if (InnerInnerSnapShot.getKey().equals(Subject)) {
                                            // Oh no were doing to deep! We now are iterating through the decks and total deck count.
                                            for (DataSnapshot DeckSnapShot : InnerInnerSnapShot.getChildren()) {
                                                if (!DeckSnapShot.getKey().equals("Total Decks")) {
                                                    // Add them to the decklist
                                                    // Update the deck spinner
                                                    DeckString.add(DeckSnapShot.getKey());
                                                    DeckAdapter.notifyDataSetChanged();
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }

    }

