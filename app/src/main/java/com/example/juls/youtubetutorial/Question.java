package com.example.juls.youtubetutorial;

public class Question {
    // Question class for study class.

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getCorrectness() {
        return Correctness;
    }

    public void setCorrectness(String correctness) {
        Correctness = correctness;
    }

    String Question;
    String Answer;
    String Correctness;

}
