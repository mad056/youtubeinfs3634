package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.widget.Toast.LENGTH_LONG;

public class EditCard extends Activity {
String ChangingSubject;
String ChangingDeck;
String ChangingName;
String ChangingDes;
String Context;
Button ChangeButton;
EditText QuestionText;
EditText AnswerText;
InputChecksEssentials Check = new InputChecksEssentials();
Login login = new Login();
Editor editor = new Editor();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);


        // Grab the intent
        ChangingSubject = getIntent().getStringExtra("Subject");
        ChangingDeck = getIntent().getStringExtra("Deck");
        ChangingName = getIntent().getStringExtra("Name");
        ChangingDes = getIntent().getStringExtra("Des");
        Context = getIntent().getStringExtra("Context");


        ChangeButton = findViewById(R.id.UpdateButton);
        QuestionText = findViewById(R.id.QuestionEditText);
        AnswerText = findViewById(R.id.AnswerMultiLineEdt);


        // We also use this class to add. We use the intent context to set that
        if (Context.equals("ADD")) {
            ChangeButton.setText("Add");
        } else {
            QuestionText.setText(ChangingName);
            AnswerText.setText(ChangingDes);
        }




    }

    public void ChangeClick(View v) {
        boolean QuestionBoolean = Check.nonNull(QuestionText.getText().toString());
        boolean AnswerBoolean = Check.nonNull(AnswerText.getText().toString());
        if (QuestionBoolean == false && AnswerBoolean == false) {
            if (Context.equals("ADD")) {

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + ChangingSubject + "/" + ChangingDeck + "/Questions");
                mdatabaseReference.child(QuestionText.getText().toString()).setValue(AnswerText.getText().toString());
                Toast.makeText(EditCard.this, "Added", LENGTH_LONG).show();
                GrabDeckToUpdate(ChangingSubject, ChangingDeck);

            } else {
                // Delete card
                FirebaseDatabase data = FirebaseDatabase.getInstance();
                DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/Subjects/" + ChangingSubject + "/" + ChangingDeck);

                MaxDatabase.child("Questions").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                            if (answerSnapshot.getKey().equals(ChangingName)) {
                                if (answerSnapshot.getValue(String.class).equals(ChangingDes)) {
                                    answerSnapshot.getRef().removeValue();


                                }

                            }


                        }


                        ReadCard();

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


                //finish();


            }


            } else {
                Toast.makeText(EditCard.this, "Please Check Your Inputs!", LENGTH_LONG).show();

            }



    }
    public void ReadCard() {

        // Add the card back in
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + ChangingSubject + "/" + ChangingDeck + "/Questions");
        mdatabaseReference.child(QuestionText.getText().toString().trim()).setValue(AnswerText.getText().toString().trim());

        // toast user and start the editor again with the same deck
        Toast.makeText(EditCard.this, "Changed", LENGTH_LONG).show();
        Intent intent = new Intent(getBaseContext(), Editor.class);
        intent.putExtra("Subject", ChangingSubject);
        intent.putExtra("Deck", ChangingDeck);
        startActivity(intent);
    }




    public void GrabDeckToUpdate(final String Subject, final String Deck) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + Login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    if (answerSnapshot.getKey().equals("Total Cards")) {
                        Log.d("CardUp", "SetDeckUpdate");
                        SetDeckUpdate(Subject, answerSnapshot.getValue(Integer.class), Deck) ;


                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SetDeckUpdate(String Subject, int TotalDecks, String Deck) {
        Log.d("Create", "SetDeckUpdateInwards");

        TotalDecks = TotalDecks + 1;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject + "/" + Deck);
        mdatabaseReference.child("Total Cards").setValue(TotalDecks);

    }

}
