package com.example.juls.youtubetutorial;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class CardItemAdapter extends RecyclerView.Adapter<CardItemAdapter.CardViewHolder> {
    ArrayList<CardItemRecyclerView> mViewHolderCardList;
    private static final String TAG = "Adapter";

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        // Set the resources found in carditem.xml
        private TextView TextViewLine1;
        private TextView TextViewLine2;
        private TextView TextViewNumberText;
        private ImageButton TheEditImageButton;


        public CardViewHolder(View itemView) {
            super(itemView);

            // Set the resources found in carditem.xml

            TextViewLine1 = itemView.findViewById(R.id.Line1Text);
            TextViewLine2 = itemView.findViewById(R.id.Line2Text);
            TextViewNumberText = itemView.findViewById(R.id.NumberView);
           TheEditImageButton = (ImageButton)  itemView.findViewById(R.id.EditImageButton);

        }
    }

    public CardItemAdapter(ArrayList<CardItemRecyclerView> ViewHolderCardList) {

        // Set from the class that sets the adapter
        mViewHolderCardList = ViewHolderCardList;
    }


    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
           // Attach our card item layout
             final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carditem, parent, false);
        // Set and return the viewholder with the correct layout.
             final CardViewHolder CIA = new CardViewHolder(v);



             // We set a on click on the image button in the adapter
        CIA.TheEditImageButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(final View view) {

                // create a popup menu
                PopupMenu popup = new PopupMenu(CIA.TheEditImageButton.getContext(),v);
                MenuInflater inflater = popup.getMenuInflater();

                // use the button menu in /res/manu
                inflater.inflate(R.menu.button_menu, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int tempid = item.getItemId();
                        Log.i(TAG, String.valueOf(item.getItemId()));

                        // grab the information of the clicked resource
                        String tempname = mViewHolderCardList.get(CIA.getAdapterPosition()).getLine1();
                        String tempDes = mViewHolderCardList.get(CIA.getAdapterPosition()).getLine2();
                        String TempSub = mViewHolderCardList.get(CIA.getAdapterPosition()).getSubject();
                        String TempDeck = mViewHolderCardList.get(CIA.getAdapterPosition()).getDeck();

                        Editor meditor = new Editor();
                        switch (tempid) {
                            // switch statement of which menu item was selected
                            case R.id.i1:
                                // delete the card
                                meditor.DeleteCard(tempname, tempDes, TempSub, TempDeck);
                                break;
                            case R.id.i2:
                                // edit the card
                                Intent intent2 = new Intent(view.getContext(), EditCard.class);
                                intent2.putExtra("Subject", TempSub);
                                intent2.putExtra("Deck", TempDeck);
                                intent2.putExtra("Name", tempname);
                                intent2.putExtra("Des", tempDes);
                                intent2.putExtra("Context", "Edit");

                                view.getContext().startActivity(intent2);

                                break;

                        }
                        return false;
                    }
                });

            }

        });


        return CIA;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, final int position) {

        // set each tiem as they are passed through
        holder.TextViewLine1.setHorizontallyScrolling(true);
        // disable word wrap haha
        holder.TextViewLine2.setHorizontallyScrolling(true);



        CardItemRecyclerView CurrentItem = mViewHolderCardList.get(position);

// set values
            holder.TextViewLine1.setText(CurrentItem.getLine1());
        holder.TextViewLine2.setText(CurrentItem.getLine2());
        holder.TextViewNumberText.setText(CurrentItem.getNumberText());

        if (CurrentItem.getSubject().equals("Youtube")) {
            holder.TheEditImageButton.setVisibility(View.INVISIBLE);
            holder.TheEditImageButton.setEnabled(false);

        }


    }

    @Override
    public int getItemCount() {
        return mViewHolderCardList.size();
    }



}
