package com.example.juls.youtubetutorial;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class MainPage extends AppCompatActivity {
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        startService(new Intent(getBaseContext(), BackgroundTask.class));
        dialog = new ProgressDialog(MainPage.this);
        CheckDefault();
    }



    public void MainPageInterActiveStudyClick(View v) {
        startActivity(new Intent(MainPage.this, InteractiveDeckPlaylistSelect.class));

    }


    public void MainPageEditorClick(View v) {



                // Popup for context manu
                PopupMenu popup = new PopupMenu(MainPage.this,v);
                MenuInflater inflater = popup.getMenuInflater();


                // Use editor menu
                inflater.inflate(R.menu.editor_menu, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int tempid = item.getItemId();
                        Log.i("MainPage", String.valueOf(item.getItemId()));

                        switch (tempid) {
                            case R.id.i1:
                                // Edit cards
                                startActivity(new Intent(MainPage.this, DeckEditSelect.class));
                                break;
                            case R.id.i2:
                                // Create  a deck
                                startActivity(new Intent(MainPage.this, CreateDeck.class));

                                break;
                            case R.id.i3:
                                // create subject
                                startActivity(new Intent(MainPage.this, CreateSubject.class));

                                break;

                        }
                        return false;
                    }
                });






    }

    public void StudyClick(View v) {
        startActivity(new Intent(MainPage.this, DeckSelectStudy.class));

    }
    public void PendingDeckClick(View v) {
        startActivity(new Intent(MainPage.this, PendingDecks.class));

    }

    public void DialogControl(String Control) {
        // Control the progress dialog for transfering default decks

        if (Control.equals("Show")) {
            dialog.setMessage("Loading Default Decks..Please Wait. ");
            dialog.setCancelable(false);
            dialog.show();
        } else {
            dialog.dismiss();

        }
    }

    public void PromptDefaultDeck() {
        // Perform action on click
         AlertDialog.Builder builder = new AlertDialog.Builder(MainPage.this);

        builder.setTitle("Default Decks");
        builder.setMessage("We provide sample content for INFS3617. You have not loaded the data. Please click yes to load it.");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                GrabQuestions();

            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }



    /// Degault deck checks
    public void CheckDefault() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference Adding = database.getReference("Users/" + Login.CurrentUserID  + "/Subjects"  );

        Adding.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean check = false;
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    if (data.getKey().equals("INFS3617")) {
                        check = true;
                        Log.d("MainPage", "Exists");
                        break;
                    }
                }
                if (!check) {
                    PromptDefaultDeck();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });

    }

    public void  GrabQuestions() {
        DialogControl("Show");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference Adding = database.getReference();

        // Now this is efficiency. A database connection inside a database connection.

        Adding.child("Default Deck").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Count the cards
                java.util.ArrayList<Question> QuestionList = new ArrayList<>();
                int i = 0;
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    String CurrentDeck = answerSnapshot.getKey();
                    i +=1;
                    Log.d("MainPage", dataSnapshot.getKey());

                    QuestionList.clear();
                    for (DataSnapshot QuestionSnapShot : answerSnapshot.getChildren()) {
                        Question tempQuestion = new Question();
                        tempQuestion.setQuestion(QuestionSnapShot.getKey());
                        tempQuestion.setAnswer(QuestionSnapShot.getValue(String.class));
                        QuestionList.add(tempQuestion);

                    }
                    WriteCards(CurrentDeck, QuestionList, i);


                }
                DialogControl("Cancel");
                Toast.makeText(MainPage.this, "Loaded!", LENGTH_LONG).show();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void WriteCards(String CurrentDeck, ArrayList<Question> Questions, int i) {


        FirebaseDatabase database2 = FirebaseDatabase.getInstance();
        final DatabaseReference DeckTotal = database2.getReference("Users/" + Login.CurrentUserID + "/Subjects/"+  "INFS3617/"  );
        DeckTotal.child("Total Decks").setValue(i);

        FirebaseDatabase database3 = FirebaseDatabase.getInstance();
        final DatabaseReference CardTotal = database3.getReference("Users/" + Login.CurrentUserID + "/Subjects/"+  "INFS3617/" + CurrentDeck  );
        CardTotal.child("Total Cards").setValue(Questions.size());
// Questions are passed through and written Step byyy steppp
        Log.d("DefaultDeckGrabber", "WRiteCard");

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + Login.CurrentUserID  + "/Subjects/" + "INFS3617/" + CurrentDeck + "/Questions");


        for (Question element : Questions) {

            mdatabaseReference.child(element.getQuestion()).setValue(element.getAnswer());


        }
    }
    }




