package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import static android.widget.Toast.LENGTH_LONG;

public class ForgotPassword extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        final EditText EmailEditText = findViewById(R.id.EmailEditText);
    Button SendButton = findViewById(R.id.ForgotPwButton);
        SendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // We create a link with the firebase auth forogt password.
                FirebaseAuth.getInstance().sendPasswordResetEmail(EmailEditText.getText().toString().trim())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    // firebase replies its successful it means the email was sent
                                    Toast.makeText(ForgotPassword.this, "Sent!", LENGTH_LONG).show();
                                    Log.d("ForgotPW", "Email sent.");
                                    finish();

                                }
                            }
                        });            }
        });

    }
}
