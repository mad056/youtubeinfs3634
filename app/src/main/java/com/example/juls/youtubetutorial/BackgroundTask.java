package com.example.juls.youtubetutorial;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BackgroundTask extends Service {
    // Run this service to check for pending transfers.
 int CurrentDeckNumberList;
    Login login = new Login();
    Handler handler = new Handler();
    Runnable turnBlack = new Runnable() {
        @Override
        public void run() {

            // Delay this for 30 minutes or 500000ms
            handler.postDelayed(turnBlack, 500000);

            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID);
            CurrentDeckNumberList = 0;

            // Check if there are any pending decks in the database
            MaxDatabase.child("PendingDeck").addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        // Checking Done!
                        CurrentDeckNumberList += 1;
                    }
                    if (CurrentDeckNumberList > 0) {
// Send notification
                        SendNotification();
                    }

                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    };


    public void SendNotification() {
        Runnable runnable = new Runnable() {
            // Running on a thread. Cause UI is already a cluster
            @Override
            public void run() {
                Intent notificationIntent = new Intent(BackgroundTask.this, PendingDecks.class);

                // Generic notification builder
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(BackgroundTask.this, "1")
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark) //icon
                        .setContentTitle("You Have Pending Decks")
                        .setContentText("You Have Pending Transfers. Click Here To See.")
                        .setContentIntent(PendingIntent.getActivity(BackgroundTask.this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT))
                        .setAutoCancel(true)//swipe for delete
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, mBuilder.build());
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    @Override
    // Let it continue running until it is stopped.
    public int onStartCommand(Intent intent, int flags, int startId) {

        // We delay the first check by 5 seconds to free up memory
        handler.postDelayed(turnBlack, 5000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
