package com.example.juls.youtubetutorial;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.example.juls.youtubetutorial.CreateDeck;

import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class CreateDeck extends Activity {
    Spinner SubjectSpinner;
    EditText DeckName;
    Login login = new Login();
    ArrayList<String> list;
    ArrayAdapter<String> dataAdapter;
    java.util.ArrayList<String> strings;
    String PendingDeckString;
    Button CreateButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_deck);

        SubjectSpinner = findViewById(R.id.CreateSelectSubSpinner);
        CreateButton = findViewById(R.id.CreateDeckButton);
        DeckName = findViewById(R.id.DeckNameEditText);

        // We ultlize this class for transfering new decs too!
        // we get the intent.
        PendingDeckString = getIntent().getStringExtra("Subject");

        // check if its null
        // Kinda a dodgy way of doing it..i know
 try {
            if (!PendingDeckString.isEmpty()) {
                CreateButton.setText("Transfer");

            }
    }catch (NullPointerException e)

    {
    }

// set up the adapters for the spinners.
        strings = new java.util.ArrayList<>();
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strings);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SubjectSpinner.setAdapter(dataAdapter);
        LoadSpinner();
    }

    // You'll probably see this similar code everywhere.
    // Iterate through the users database, adding only the subject names.
    // Chuck a look at the documentation included. It includes  a picture of our database structure. That will help! :')

    public void LoadSpinner() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    // if the user is our current user
                    if (answerSnapshot.getKey().equals(login.CurrentUserID)) {

                        // A for inside a for inside a for. So if the key is the subjects we then NEED to iterate one more time :(
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Subjects")) {
                                for (DataSnapshot InnerInnerSnapShot : InnerSnapshop.getChildren()) {
                                    strings.add(InnerInnerSnapShot.getKey());
                                    dataAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }

    // Looks familiar. Yeah i am using a similar format to the create subject class.
    // Grab Values, start databsae connection, insert into the right location.
    // From looking at the mdatabaseReference you can get a gist of how my database is structured.
    public void CreateDeckOnClick(View v) {
        try {
            String TempDeckName = DeckName.getText().toString().trim();
            String ChosenSpinner = SubjectSpinner.getSelectedItem().toString();
            try {
                if (!PendingDeckString.isEmpty()) {
                    CheckDeck(TempDeckName, ChosenSpinner);
                }
            }catch (NullPointerException e) {
                CheckDeck(TempDeckName, ChosenSpinner);
            }
        } catch (NullPointerException e) {
            Toast.makeText(CreateDeck.this, "Nothing Selected!", LENGTH_LONG).show();

        }

        // If its a transfer we load up the deck from pending and transfer it.




    }

    public void CheckDeck(final String mDeck, final String mSubject) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/"+ mSubject  );

        MaxDatabase.child(SubjectSpinner.getSelectedItem().toString()).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean check = false;
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        if (answerSnapshot.getKey().equals(DeckName.getText().toString().trim())) {
                            // Check if the deck name exists
                            check = true;
                        }

                }
                if (check) {
                    // inform the user they
                    Toast.makeText(CreateDeck.this, "Error. Deck already exists.", LENGTH_LONG).show();
                } else {
                    try {
                        if (!PendingDeckString.isEmpty()) {
                            // We use the same class to transfer thus if its a transwer we transfer instead of create
                            TransferDeck();
                        }
                    }catch (NullPointerException e) {
                        // Create deck on null point :)
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + mSubject + "/" + mDeck);
                        mdatabaseReference.child("Total Cards").setValue(0);
                        GrabDeckToUpdate(mSubject);
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void TransferDeck() {
        String TempDeckName = DeckName.getText().toString().trim();
        String ChosenSpinner = SubjectSpinner.getSelectedItem().toString();
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID + "/PendingDeck");


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference Adding = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" +ChosenSpinner + "/" +  TempDeckName);


        // Now this is efficiency. A database connection inside a database connection.

        MaxDatabase.child(PendingDeckString).addValueEventListener(new ValueEventListener() {
            int i = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Count the cards

                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    Adding.child("Questions").child(answerSnapshot.getKey()).setValue(answerSnapshot.getValue(String.class));
                    i +=1;

                }
                // Write the count
                Adding.child("Total Cards").setValue(i);
                DeletePendingDeck();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void DeletePendingDeck() {

        // Remove value.
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/PendingDeck/" +  PendingDeckString);
        mdatabaseReference.removeValue();
        Toast.makeText(CreateDeck.this, "Transferred!", LENGTH_LONG).show();

        finish();
    }

    public void GrabDeckToUpdate(final String Subject) {
        // We up the deck count

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference MaxDatabase = database.getReference();

        MaxDatabase.child("Users/" + login.CurrentUserID + "/Subjects/" + Subject).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        if (answerSnapshot.getKey().equals("Total Decks")) {
                            Log.d("Create", "SetDeckUpdate");
                            // grab the deck count.
                            // send it to another method ot up the count
                            SetDeckUpdate(Subject, answerSnapshot.getValue(Integer.class)) ;


                        }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SetDeckUpdate(String Subject, int TotalDecks) {
        Log.d("Create", "SetDeckUpdateInwards");

        // hey its the method that up the counts
        // pretty self explanatory
        TotalDecks = TotalDecks + 1;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/Subjects/" + Subject );
        mdatabaseReference.child("Total Decks").setValue(TotalDecks);
     //   System.out.println(String.valueOf("Second " + TotalDecks));

        // The creating would finish here. We kill the dialog and toast hte user
        finish();
        Toast.makeText(CreateDeck.this, "Created!", LENGTH_LONG).show();

    }
    }