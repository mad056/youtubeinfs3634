package com.example.juls.youtubetutorial;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class PendingDecks extends AppCompatActivity {
Login login = new Login();
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView DeckList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_decks);
        DeckList = findViewById(R.id.PendingTransferListView);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(PendingDecks.this, R.layout.mylist, arrayList);
        DeckList.setAdapter(adapter);
        CheckPending();

        DeckList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String str = DeckList.getItemAtPosition(position).toString();

                AlertDialog.Builder builder = new AlertDialog.Builder(PendingDecks.this);

                // on click we prompt the user to transfer

                builder.setTitle("Confirm");
                builder.setMessage("Do you want to accept this transfer?");

                builder.setPositiveButton("Transfer", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Yes we use create deck class.
                        Intent intent = new Intent(getBaseContext(), CreateDeck.class);
                        intent.putExtra("Subject", str);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Delete the pending transfer
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference mdatabaseReference = database.getReference("Users/" + login.CurrentUserID + "/PendingDeck/" +  str);
                        mdatabaseReference.removeValue();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }

        });
    }

    @Override
    public void onResume(){
        super.onResume();
        CheckPending();

    }
    public void CheckPending() {
        // grab and load adapter / listview with pending decks in hte database

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference("Users/" + login.CurrentUserID  );
        arrayList.clear();


        MaxDatabase.child("PendingDeck").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    arrayList.add(answerSnapshot.getKey());
                    adapter.notifyDataSetChanged();


                }
                if (arrayList.size() == 0) {
                    Toast.makeText(PendingDecks.this, "No Current Pending Decks", LENGTH_LONG).show();

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void SelectTransferDeckClick(View v) {
        startActivity(new Intent(PendingDecks.this, TransferDeckSelect.class));

    }
}
