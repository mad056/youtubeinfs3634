package com.example.juls.youtubetutorial;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Logger;

public class YoutubePlaylistFragment extends Fragment {
Video data;
    Login login = new Login();
String Playlist;
    private RecyclerView mRecycleView;
    private RecyclerView.Adapter mAdapter;
    ArrayList<CardItemRecyclerView> CardList;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final Logger LOGGER = Logger.getLogger(Editor.class.getName());
    public YoutubePlaylistFragment(){};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View PageTwo = inflater.inflate(R.layout.activity_youtube_playlist_fragment, container, false);


        CardList = new ArrayList<>();
        mRecycleView = PageTwo.findViewById(R.id.YoutubeFragmentRecycle);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new CardItemAdapter(CardList);
        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);

        if (getArguments() != null) {
            Playlist = getArguments().getString("Playlist");

        }
        // Adding a divider to each item.
        mRecycleView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        LoadData(Playlist);
        return PageTwo;
    }


    public void MakeRequest(String i) {
        // Hey its like our tutorial. Thanks INFS3534 tutorial for introducing me to GSOn


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key= {API key} &maxResults=50 &playlistId = {id}

        // Youtube HTTP API URL for playlists
        String apikey  = Config.YOUTUBE_API_KEY;
        String url ="https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key=";

        // Second consturctor
        String url2 = "&maxResults=50&playlistId=";

        // Format it
        String FormattedString = i.replaceAll(" ", "+");

        String FinalURL = url + apikey + url2 + FormattedString;
        Log.d("Youtube", FinalURL);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                // Parse and grab the JSON result
                (Request.Method.GET, FinalURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Youtube", response.toString());

                        Gson g = new Gson();

                        data = g.fromJson(response.toString(), Video.class);
                        Log.d("Youtube", "Done");
                        int tempint = 1;

                        for (items i : data.getItemlist()) {
                            // The items are the youtube videos in the playlist.
                            // we use the adapter to add them to the recycle view

                            CardItemRecyclerView tempitem = new CardItemRecyclerView(i.getSnippet().getTitle(), i.getSnippet().getDescription(), String.valueOf(tempint), "Youtube", "Video");
                            CardList.add(tempitem);
                            tempint +=1;
                            mAdapter.notifyDataSetChanged();

                        }

                        Log.d("Youtube", data.getKind());
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }


                });

//Access the RequestQueue through your singleton class.
        queue.add(jsonObjectRequest);
    }
    public void LoadData(final String id ) {
        // You might be thinking...
        // Why so many for loops.
        // Now i definiely can just go stright to the Reference.
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference( );

        MaxDatabase.child("Playlist").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

           if (answerSnapshot.getKey().equals(Playlist)) {
               MakeRequest(answerSnapshot.getValue(String.class));
           }
                    // Debug

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}
